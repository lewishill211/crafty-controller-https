NGINX & Let's Encrypt

Why would you want to access your domain securely?

    The information between you and your server is encrypted
    Enables the owner to access their Crafty Dashboard domain at work (depends on rules)
    Adds extra layers of security by preventing nonsecure information being obtained by third parties
    Prevents man-in-the-middle attacks
    Provides a high degree of confidence that you are accessing you server

Utilizing NGINX, your server can obtain an https secure certificate from Lets Encrypt for free.


1.) Setting up DNS
Create an A record pointing to the IP Crafty Controller is on. Most popular Registrars will have a DNS section that you can edit or if you have it managed by another company such as Cloudflare, then you would change it in there. For example, the domain could be crafty.example.com so the A record will be crafty and the ipv4 IP will be your crafty IP..


2.) Install Cerbot!

    sudo apt install certbot -y


3.) Get certificate from Lets Encrypt

    sudo certbot certonly --standalone --agree-tos --email {EMAIL} -d {DOMAIN}

Replace {EMAIL} with your valid email and {DOMAIN} with the domain you setup in step 1.

(make sure you port forward 80 and 443 to server IP or open them up. If you do not Let's Encrypt will not work and you only have 5 trys. Then you will have to wait 7 days.)


The next part is optional, and will allow you to connect to your Crafty Dashboard without specifying port 8000.

4.) Install NGINX

    sudo apt install nginx -y


5.) Remove Default configuration file

    sudo rm /etc/nginx/sites-enabled/default

6.) Create new NGINX conf file

    sudo nano /etc/nginx/conf.d/crafty.conf


7.) Copy and paste my crafty.conf file into yours and change crafty.example.com to your's domain name.


8.) Test new NGINX conf file

    sudo nginx -t


9.)if successful restart NGINX

    sudo systemctl restart nginx


10.) Now log into your new crafty Dashboard without the port will be something like https://crafty.example.com.

